# Student Registration Client

## Demo Link 
Live: https://student-registration-app.onrender.com/

## Introduction
Student Registration is an app that is used to register students to an organization. It has a basic listing screen where list of students are displayed in a table, a basic form to get / edit the students. We can also delete a student. This web application is build using MERN Stack. This repo contains the frontend code. I've used ReactJs for the same.

## Screenshots
Listing: https://screenrec.com/share/Dv4smOr8py   
Add/Edit: https://screenrec.com/share/ZAWQ9k6YKx  
Delete: https://screenrec.com/share/cuL2OHIQN8 

## Tech Stacks
`HTML` `CSS` `JavaScript` `ReactJs` `SCSS` `BEM Methodology` `Axios` 

## Approach
I have used `ReactJs` for this web application. I have split the application into several components: input-form, listing-table, top-bar, toaster and main. Main component is the parent component where all other components are grouped and rendered. This is the component where all the API calls are handled. I have adopted `SCSS` styling and `BEM` naming style for my `CSS` class names to style the `HTML` pages and components. I've used `Axios` library for API calls.

## Setup
* Clone the git repository
* Open command prompt and go to the cloned directory
* Run the below command to install all the dependancies 
```sh
npm install
```
* This will create node_modules and package.json file.
* To start the app run the below command
```sh
npm start
```

## Acknowledgements
* [ReactJs](https://reactjs.org/) 
* [BEM Methodology](https://en.bem.info/methodology/) 
* [SASS Tutorial](https://www.w3schools.com/sass/) 
* [Deploy Code to Render.com](https://javascript.plainenglish.io/how-to-deploy-a-react-application-to-render-611ef3aca84a#:~:text=We%20can%20do%20that%20by,the%20Manual%20Deploy%20dropdown%20menu.&text=Finally%2C%20we%20can%20visit%20the,can%20deploy%20your%20React%20Applications.)
* [Render](https://render.com/docs)

## Contact
* meeravignesh2011@gmail.com
* 9790391085




