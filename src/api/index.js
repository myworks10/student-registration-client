import axios from "axios";

const api = axios.create({
  baseURL: "https://student-registration.onrender.com/api",
});

export const insertStudent = (payload) => api.post(`/student`, payload);
export const studentList = () => api.get("/students");
export const editStudent = (id, payload) => api.put(`/student/${id}`, payload);
export const deleteStudent = (id) => api.delete(`/student/${id}`);

const apis = {
  insertStudent,
  studentList,
  editStudent,
  deleteStudent,
};

export default apis;
