import "./toaster.scss";

const Toaster = (props) => {
  const { success, message, closeToaster } = props;
  return (
    <div className="toaster">
      {success ? (
        <img src="assets/images/ok.svg" alt="ok" />
      ) : (
        <img src="assets/images/cancel.svg" alt="cancel" />
      )}
      <div className="toaster__content">
        <p className="toaster__content-heading">
          <strong>
            {success
              ? "Yay! Everything Worked Successfully"
              : "Oops! Something Went Wrong"}
          </strong>
        </p>
        <p className="toaster__content-message">{message}</p>
      </div>
      <span className="toaster__close" onClick={() => closeToaster()}>
        <img src="assets/images/close.svg" alt="close"></img>
      </span>
    </div>
  );
};

export default Toaster;
