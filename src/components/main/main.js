import { useState, useEffect } from "react";
import { mobileRegex, emailRegex, tableHeader } from "../../common/constants";
import StudentList from "../student-list/student-list";
import Toaster from "../toaster/toaster";
import TopBar from "../topbar/topbar";
import "./main.scss";
import api from "../../api";
import StudentForm from "../student-form/student-form";

const Main = () => {
  const [id, setId] = useState("");
  const [toasterObj, setToasterObj] = useState({
    message: "",
    success: false,
  });
  const [toasterOpen, setToasterOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [addNew, setAddNew] = useState(false);
  const [studentList, setStudentList] = useState([]);
  const [createObj, setCreateObj] = useState({
    id: "",
    name: "",
    mobile: "",
    email: "",
  });
  const [disableButton, setDisableButton] = useState(true);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    reset();
  }, []);

  useEffect(() => {
    if (toasterOpen) {
      setTimeout(() => closeToaster(), 3000);
    }
  }, [toasterOpen]);

  /**
   * Side effect to Disable Button
   */
  useEffect(() => {
    setDisableButton(checkDisabled());
  }, [createObj]);

  /**
   * Handle Toaster Open
   * @param {boolean} handler Toaster Open / Close 
   * @param {boolean} toastState Toaster Succes / Fail Boolean 
   * @param {string} message Toaster Message
   */
  const handleToaster = (handler ,toastState, message) =>{
    setToasterOpen(handler);
    setToasterObj({ success: toastState, message: message });
  }

  /**
   * Edit / Delete Action Button Click on Listing
   * @param {any} obj 
   */
  const handleButtonClick = (obj) => {
    if (obj?.type === "edit") {
      setId(obj?.item?._id);
      const { name, id, mobile, email } = obj?.item;
      setAddNew(true);
      setEditMode(true);
      setCreateObj({ name, id, email, mobile });
    } else if (obj?.type === "delete") {
      setIsLoading(true);
      api
        .deleteStudent(obj?.item?._id)
        .then((res) => {
          if (res?.data?.success) {
            reset();
            handleToaster(true, true, res?.data?.message);
          } else {
            handleToaster(true, false, res?.message);
            setIsLoading(false);
          }
        })
        .catch((err) => {
          handleToaster(true, false, err?.response?.data?.message);
          setIsLoading(false);
        });
    }
  };

  /**
   * Student List API
   */
  const getStudentList = () => {
    setIsLoading(true);
    api
      .studentList()
      .then((res) => {
        if (res?.data?.success) {
          setStudentList(res?.data?.data);
          setIsLoading(false);
        } else {
          setIsLoading(false);
        }
      })
      .catch((err) => {
        handleToaster(true, false, err?.response?.data?.message);
        setIsLoading(false);
      });
  };

  /**
   * Handle New Student Register Button Click
   */
  const handleNewClick = () => {
    setAddNew(true);
  };

  /**
   * Register / Edit Button Enable/ Disable
   * @returns Boolean to disable / enable button
   */
  const checkDisabled = () => {
    return (
      !createObj?.id ||
      !createObj?.name ||
      !createObj?.mobile ||
      !createObj?.email ||
      !createObj?.email?.match(emailRegex) ||
      !createObj?.mobile?.match(mobileRegex)
    );
  };

  /**
   * Handle Add / Edit Submit
   */
  const submitRegistration = () => {
    if (!editMode) {
      setIsLoading(true);
      api
        .insertStudent(createObj)
        .then((res) => {
          if (res?.data?.success) {
            handleToaster(true, true, res?.data?.message);
            reset();
          } else {
            handleToaster(true, false, res?.message);
            setIsLoading(false);
          }
        })
        .catch((err) => {
          handleToaster(true, false, err?.response?.data?.message);
          setIsLoading(false);
        });
    } else {
      setIsLoading(true);
      api
        .editStudent(id, createObj)
        .then((res) => {
          if (res?.data?.success) {
            handleToaster(true, true, res?.data?.message);
            reset();
          } else {
            handleToaster(true, false, res?.message);
            setIsLoading(false);
          }
        })
        .catch((err) => {
          handleToaster(true, false, err?.response?.data?.message);
          setIsLoading(false);
        });
    }
  };

  /**
   * Update Input Object on User Data
   * @param {any} obj Input Object
   */
  const updateCreateObj = (obj) => {
    setCreateObj((prevState) => ({
      ...prevState,
      [obj?.key]: obj?.event?.target?.value,
    }));
  };

  /**
   * Handle Back to Listing Button Click
   */
  const backToListing = () => {
    setAddNew(false);
    setId("");
    setCreateObj({ id: "", name: "", mobile: "", email: "" });
    setAddNew(false);
    setEditMode(false);
  };

  /**
   * Close Toaster
   */
  const closeToaster = () => {
    handleToaster(false, false, "");
  };

  /**
   * Reset All States
   */
  const reset = () => {
    setId("");
    setCreateObj({ id: "", name: "", mobile: "", email: "" });
    setAddNew(false);
    setEditMode(false);
    setStudentList([]);
    getStudentList();
  };

  return (
    <>
       {isLoading && (
        <div
          className="loader modal show"
          id="formModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="formModalCenterTitle"
          aria-modal="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="spinner-border text-light" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          </div>
        </div>
      )}
      {toasterOpen && (
        <Toaster
          success={toasterObj?.success}
          message={toasterObj?.message}
          closeToaster={closeToaster}
        />
      )}
      <TopBar />
      {!addNew ? (
        !isLoading &&
          <StudentList
            tableHeader={tableHeader}
            tableData={studentList}
            newClick={handleNewClick}
            buttonClick={handleButtonClick}
          />
      ) : (
        <>
          <StudentForm
            editMode={editMode}
            createObj={createObj}
            onInputChange={updateCreateObj}
            disableButton={disableButton}
            backToListing={backToListing}
            submitRegistration={submitRegistration}
          />
        </>
      )}
    </>
  );
};

export default Main;
