import "./topbar.scss";

const TopBar = () => {
  return (
    <div className="topBar">
      <span className="topBar__identity">
        <strong>SR</strong>
      </span>
      <h1 className="topBar__heading">Student Registration Form</h1>
    </div>
  );
};

export default TopBar;
