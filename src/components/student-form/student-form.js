import "./student-form.scss";
import { emailRegex, mobileRegex } from "../../common/constants";

const StudentForm = (props) => {
  const {
    createObj,
    onInputChange,
    editMode,
    backToListing,
    submitRegistration,
    disableButton,
  } = props;
  return (
    <div className="form">
      <div className="form__data">
        <strong><p>Enter the Details</p></strong>
        <div className="form__data-user-input">
          <label>
            Student ID<sup>*</sup>
          </label>
          <input
            data-testid="studentid"
            disabled={editMode}
            value={createObj?.id}
            type="text"
            placeholder="Enter Student ID"
            onChange={(event) => onInputChange({ key: "id", event })}
          ></input>
        </div>
        <div className="form__data-user-input">
          <label>
            Student Name<sup>*</sup>
          </label>
          <input
            data-testid="name"
            value={createObj?.name}
            type="text"
            placeholder="Enter Name"
            onChange={(event) => onInputChange({ key: "name", event })}
          ></input>
        </div>
        <div className="form__data-user-input">
          <label>
            Student Mobile<sup>*</sup>
          </label>
          <input
            data-testid="mobile"
            value={createObj?.mobile}
            type="text"
            placeholder="Enter Mobile Number"
            onChange={(event) => onInputChange({ key: "mobile", event })}
          ></input>
          {(createObj?.mobile?.length>0 && !createObj?.mobile?.match(mobileRegex)) && <span> Enter Valid Mobile Number</span>}
        </div>
        <div className="form__data-user-input">
          <label>
            Student Email<sup>*</sup>
          </label>
          <input
            data-testid="email"
            value={createObj?.email}
            type="text"
            placeholder="Enter Email"
            onChange={(event) => onInputChange({ key: "email", event })}
          ></input>
          {(createObj?.email?.length>0 && !createObj?.email?.match(emailRegex)) && <span> Enter Valid Email Address</span>}
        </div>
      </div>
      <div className="form__buttons">
        <button onClick={() => backToListing()}>Back To List</button>
        <button
          className={`${disableButton && "form__buttons-disabled"}`}
          onClick={() => submitRegistration()}
          disabled={disableButton}
        >
          {editMode ? "Edit" : "Register"}
        </button>
      </div>
    </div>
  );
};

export default StudentForm;
