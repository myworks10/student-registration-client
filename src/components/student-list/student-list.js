import { map } from "lodash";
import "./student-list.scss";

const StudentList = (props) => {
  const { tableHeader, tableData, buttonClick, newClick } = props;

  const tableHeadingRender = () => {
    return map(tableHeader, (head, index) => (
      <th style={{ width: head?.width }} key={index}>
        {head?.value}
      </th>
    ));
  };

  const tableDataRender = (ele) => {
    return map(tableHeader, (el, index) => {
      return (
        <td style={{ width: el?.width }} key={index}>
          {el?.key !== "actions" ? (
            <span>{ele[el?.key]}</span>
          ) : (
            <div className="list-buttons">
              <span onClick={() => buttonClick({ item: ele, type: "edit" })}>
                <img src="assets/images/edit.png" alt="edit" />
              </span>
              <span onClick={() => buttonClick({ item: ele, type: "delete" })}>
                <img src="assets/images/delete.svg" alt="delete" />
              </span>
            </div>
          )}
        </td>
      );
    });
  };

  return (
    <div className="list">
      {tableData?.length > 0 ? (
        <>
          <button className="list__new float-right" onClick={() => newClick()}>
            Add New
          </button>
          <table>
            <thead>
              <tr>{tableHeadingRender()}</tr>
            </thead>
            <tbody>
              {map(tableData, (ele, index) => (
                <tr key={index}>{tableDataRender(ele)}</tr>
              ))}
            </tbody>
          </table>
        </>
      ) : (
        <>
          <img src="assets/images/no-data.png" alt="no-data"/>
          <p>No Records Found</p>
          <button className="list__new" onClick={() => newClick()}>Add New</button>
        </>
      )}
    </div>
  );
};

export default StudentList;
