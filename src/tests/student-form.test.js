import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import StudentForm from '../components/student-form/student-form';

describe('Student Form', ()=>{
    test('render input fields', () => {
        render(<StudentForm />);
        const idinput = screen.getByTestId('studentid');
        const nameinput = screen.getByTestId('name');
        const mobileinput = screen.getByTestId('mobile');
        const emailinput = screen.getByTestId('email');
        expect(idinput).toBeInTheDocument();
        expect(nameinput).toBeInTheDocument(); 
        expect(mobileinput).toBeInTheDocument();
        expect(emailinput).toBeInTheDocument();
    });
   /*  test('pass value to input fields', () => {  
        render(<StudentForm />);
        const idinput = screen.getByTestId('studentid');
        const nameinput = screen.getByTestId('name');
        const mobileinput = screen.getByTestId('mobile');
        const emailinput = screen.getByTestId('email');
        userEvent.type(idinput,'1');
        userEvent.type(nameinput,'Vicky');
        userEvent.type(mobileinput,'9798776567');
        userEvent.type(emailinput,'vicky1309@gmail.com');
        expect(screen.getByTestId('studentid')).toHaveValue('1');
        expect(screen.getByTestId('name')).toHaveValue('Vicky');
        expect(screen.getByTestId('mobile')).toHaveValue('9798776567');
        expect(screen.getByTestId('email')).toHaveValue('vicky1309@gmail.com');
    }) */
})