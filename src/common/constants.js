export const mobileRegex =  /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
export const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

export const tableHeader = [
    { key: "id", value: "Student Id", width: "100px" },
    { key: "name", value: "Name", width: "200px" },
    { key: "mobile", value: "Mobile Number", width:"200px" },
    { key: "email", value: "Email", width:"200px" },
    { key: "actions", value: "Actions", width:"100px"}
  ];